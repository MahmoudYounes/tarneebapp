# README #

this repository represents code for a game called tarneeb. the game here is Multiplayer online game implemented using node.js for server and JS for client. simply to deploy the server, run the application using node.js. to download dependencies use the .json file attached with the server folder. 

### What is this repository for? ###

* tarneeb game Multiplayer version 0.1

### How to set up? ###

* use the package.json file to download all required dependencies.
* run the server using node.js application.
* start the client.

### Who do I talk to? ###

* for any issues message m.younesbadr@gmail.com