/*
	this file has all function responsible for connecting and communicating with the server of the game.
*/


//socketio for client side.
var socket = io.connect('http://cmpassign3.cloudapp.net:5521');

window.onbeforeunload = function(event)
{
  	socket.emit('disconnect');
};

//===========================================register events==================================\\

//this event fires on first connection, which sends the actual count of the user in the lobby.
socket.on('firstConnection', function(id){
	console.log("first.");
	
	players[0].pActualCount = id;
	players[1].pActualCount = (id + 1) % 4;
	players[2].pActualCount = (id + 2) % 4;
	players[3].pActualCount = (id + 3) % 4;

	gameLog.yourId = "your id " + id;

	//console.log("your id is ", players[0].pActualCount);
});


socket.on('updateGame', function(obj){
	game.gameState  		= obj.gameState;
	game.lastPlayerCall 	= obj.lastPlayerCall;
	game.lastCall 			= obj.lastCall;	
	game.playerToCall 		= obj.playerToCall;
	game.playerNoTurn 		= obj.playerNoTurn;
	game.cutColor 			= obj.cutColor;
	game.roundNo 			= obj.roundNo;
	game.currRoundCards     = obj.currRoundCards;
	game.playerLastCapture  = obj.playerLastCapture;
	game.teamsScore     	= obj.teamsScore;
	
	/*
	players.forEach(function(player){
		if(player.pActualCount == 0)
		{
			player.capturesCount = game.pCap0;
		}
		else if (player.pActualCount == 1)
		{
			player.capturesCount = game.pCap1;	
		}
		else if (player.pActualCount == 2)
		{
			player.capturesCount = game.pCap2;	
		}
		else if (player.pActualCount == 3)
		{
			player.capturesCount = game.pCap3;	
		}
	})
	*/

	gameLog.turn 			= "turn: " + game.playerNoTurn;
	gameLog.cutColor 		= "cut color: " + strCardRank[game.cutColor];
	gameLog.playerCalled 	= "player called: " + game.playerToCall;
	gameLog.call 			= "call: " + game.lastCall;
	gameLog.playerLastCap   = "last cap: " + game.playerLastCapture;
	gameLog.team0caps 		= "team 0 has: " + obj.pCap0 + obj.pCap2;
	gameLog.team1caps 		= "team 1 has: " + obj.pCap1 + obj.pCap3;
	gameLog.team0score 		= "p0,p2 score: " + game.teamsScore[0]; 
	gameLog.team1score 		= "p1,p3 score: " + game.teamsScore[1]; 
});

socket.on('recieveCards', function(obj){
	players[0].cards = obj;
	drawCanvas();
});


socket.on('getCall', function(obj){	

	var i, options = [7, 8, 9, 10, 11, 12, 13, "pass"];
	
	var ret;
	if(game.lastCall == 0)
		ret = "first to call.\n";
    else
    	ret = "lastCall: " + game.lastCall + " by player" + game.lastPlayerCall+" .\n";
    ret += "default value: pass. \n";
	ret += "valid calls: ";
    for(i = 0;i < options.length;i++) 
    {
    	if (game.lastCall < options[i])
			ret += options[i] + " ";  
	}    

	if(game.gameState == 1 && game.playerToCall == players[0].pActualCount)
	{
		var calls = window.prompt(ret,"pass");
		if(calls == "pass") 
		{
			socket.emit('calling', [-1, 'pass']);
		}
		else if(calls != "7" && calls != "8" && calls != "9" && calls != "10" && calls != "11" && calls != "12" && calls != "13")
		{
			socket.emit('calling', [-1, 'pass']);
		}
		if(calls != 'pass' && calls != null)
		{
			console.log("player, ", players[0].pActualCount," called ",calls);
			socket.emit('calling', [1, Number(calls)]);
		}
	}
	else
	{
		console.log("recieved calling event but bypassing it.");
	}

});

socket.on('decideCutColor', function(data){
	if(game.gameState == 2 && game.lastPlayerCall == players[0].pActualCount)
	{
		ret  = "pick up cut color.\n";
		ret += "default: none.\n";
		ret += "valid options: none spade heart diamond clubs.";
		var cut = window.prompt(ret,"none");
		if (cut != "none" && cut != "spade" && cut != "heart" && cut != "diamond" && cut != "clubs")
		{
			cut = "none";
		}		
		socket.emit('playerCutColor', cardStrRank[cut]);
	}
});

var myTurn = 0;
socket.on('takeTurns', function(data){
	console.log("recieved turn request.");
	if(game.gameState == 3 && game.playerNoTurn == players[0].pActualCount)
	{
		console.log("it is my turn.");
		myTurn = 1;
	}

});

socket.on('validCard', function(obj)
{
	myTurn = 0;
	
	for (var i = 0;i < 4;i++)
	{
		if (players[i].pActualCount == obj[0])
		{
			players[i].cards[obj[1]] = obj[2];
			players[i].cardPos[obj[1]] = midTable[i]; 		
			players[i].cardInHand[obj[1]] = 0;
			players[i].cardView[obj[1]] = 0;
		}
	}

	drawCanvas();
	
});

socket.on('newRound', function(obj){
	//those loops are for a trivial task, remove card out of canvas -_- cuz i am idiot programmer -_-.
	console.log("gc: ", game.currRoundCards);
	game.currRoundCards.forEach(function(card){
		players.forEach(function(player){
			if(player.pActualCount == card[2])
			{
				for(var i = 0;i < player.cards.length;i++)
				{
					if(player.cards[i][0] == card[0] && player.cards[i][1] == card[1])
					{
						player.cardPos[i] = [canvasW + 1000, canvasH + 1000];
					}
				}
			}
		})
	})
});

socket.on('teamWon', function(obj){
	if(obj[0] == players[0].pActualCount || obj[1] == players[0].pActualCount)
	{
		window.info("you won.");
	}
	else
	{
		window.info("you lost :'(");
	}
})
