/*
	UI javascript code for tarneeb game.
	the game scheme is stored in a global game var.
*/

//==============================================global vars===================================\\


//global vars to draw on canvas
var canv = document.getElementById("myCanvas");
var ctx  = canv.getContext('2d');

//global screen inits
canv.width = document.body.clientWidth; 	//document.width is obsolete
canv.height = document.body.clientHeight;   //document.height is obsolete
canvasW = canv.width;
canvasH = canv.height;

//cards image
var img = document.createElement('img');
img.src = 'cards.gif';

//card MetaData
var cardWidth = 72;
var cardHight = 100;

var cardWidthDrw = (72 / 1152) * canvasW;
var cardHightDrw = (100 / 513) * canvasH;

//vars used for automatic bots
var autobot = 0;
var playerTurn = -1;

var cardPicRank = {
   //rnk: place in pic
	0	: 2,	//clubs
	1 	: 1,	//diamond
	2 	: 0,	//heart
	3	: 3,	//spade
	4	: 4		//none
};

//resolve string to card rank
var cardStrRank = {
	"none" 		: 4,
	"spade"		: 3,
	"heart"		: 1,
	"diamond" 	: 0,
	"clubs"		: 2
}

var strCardRank = {
	4 	: "sanz",
	3	: "spade",
	2	: "clubs",
	1	: "heart",
	0	: "diamond"
}

//metadata for cards back
var backCardData = {
	cardLoc 	: 4,	//must be 4 to get the last row in pic
	cardTyp 	: 7,	//changes card look
}

//locations of cards played at mid table.
var midTable = [
	[(canvasW / 2) , (canvasH / 2) + 50],
	[(canvasW / 2) + 50 , (canvasH / 2)],
	[(canvasW / 2) , (canvasH / 2) - 50],
	[(canvasW / 2) - 50 , (canvasH / 2)],
]

//players data
var players = [];

//game object that holds game data
//game data (should be found on server and read from there)
var game = {	
	gameState			: 0,	// 0 => not Started, 1 => currently playing (decide cuts), 2 => players take turns to play, 3 => done decide who wins.
	cutColor 			: 4,	// 0 => clubs, 1 => diamond, 2 => heart, 3 => spade, 4 => none.
	roundNo				: 0,	// preserves current round no. (0->12: 13 rounds)
	playerNoTurn		: 0,	// preserves the current player turn. (gets current player number)
	lastPlayerCall		: 0,	// preserves the last player who called.
	playerToCall		: 0,	// current player to call
	lastCall 			: 0,	// preserves the maximum count of calls
	currRoundCards  	: [],	// store cards in current round
	playerLastCapture 	: 0,
	teamsScore 			: [],
	lastCardIdxPlayed	: -1
};

//game log updated and displayed for player.
var gameLog = {
	yourId 			: "-",
	turn  			: "-",
	cutColor 		: "-",
	call 			: "-",
	playerCalled 	: "-",
	playerLastCap 	: "-",
	team0caps 		: "-",
	team1caps 		: "-",
	team0score		: "-",
	team1score 		: "-",
	props 			: ["yourId", "turn", "cutColor", "call", "playerCalled", "playerLastCap", "team0caps", "team1caps", "team0score", "team1score"],
	y 				: 0.4 * canvasH,
	x 				: 0.2 * canvasW,
	count 			: 10,
	writeText 		: function(){
		ctx.font = "10 Arial";
		for(var i = 0;i < gameLog.count;i++)
		{
			ctx.strokeText(gameLog[gameLog.props[i]], gameLog.x, (gameLog.y + i * 20));
		}
	}
}

//==============================================global funs===================================\\

/*               =======init functions=======               */
/*
	function that creates player objects and initializes them.
*/
function preparePlayers()
{
	for (var i = 0; i < 4;i++)
	{
		players.push({
			name 			: "john doe" + i,
			pActualCount	: "player?",
			playerNo 		: i,
			cards			: [],
			cardView		: [],							//0 => normal card view, 1 => back card view
			translateCards	: [0,0],						//tuple [x,y] for cards translation
			rotateCards		: 0,							//rotate cards
			capturesCount	: 0,							//3dd el lamat :D  
			cardStartPosX	: 0,							//x co-ordinate start for drawing cards
			cardStartPosY	: 0,							//y co-ordinate start for drawing cards
			cardPos 		: [],							//array of pairs of x and y indication current position of each of the 13 cards
			cardHover		: [],							//if the card is hovored or no
			cardInHand		: [],
			drawPlayerCards : function () {
				if(this.cardPos.length == 0 )
				{
					console.log("null array");
					return;
				}
				for (var i = 0;i < this.cards.length;i++) 
				{
					if (this.cardView[i] == 0 && this.cardPos[i] != null)
					{	
						ctx.save();	
						ctx.drawImage(img,
									this.cards[i][1] * cardWidth, 
									this.cards[i][0] * cardHight, 
									cardWidth, 
									cardHight, 
									this.cardPos[i][0], 
									this.cardPos[i][1], 
									cardWidthDrw, 
									cardHightDrw); 
						ctx.restore();					
					}
					else if(this.cardPos[i] != null)
					{	
						ctx.save();
						if(this.playerNo % 2 == 1 && this.cardInHand[i] = 1)
						{
							// move to the center of the canvas
						    ctx.translate(canvasW / 2, canvasH / 2);

						    // rotate the canvas to the specified degrees
						    ctx.rotate(90 * Math.PI / 180);
						}
						ctx.drawImage(img,								    //image
									backCardData.cardTyp * cardWidth,       //start cutting at (x)
									backCardData.cardLoc * cardHight, 		//start cutting at (y)
									cardWidth, 								//cut for distance of (x)
									cardHight, 								//cut for distance of (y)
									this.cardPos[i][0], 					//draw this card starting from (x)
									this.cardPos[i][1],					    //draw this card starting from (y)
									cardWidthDrw, 							//draw in width of (x)
									cardHightDrw); 							//draw in width of (y)							
						ctx.restore();
					}
				}
			}
		});
	}	

	var player = players[0];	
	//player 0 options
	player.cardStartPosX = 0.3 * canvasW;							//x co-ordinate start for drawing cards	
	player.cardStartPosY = canvasH - ((3 * cardHight) / 4);			//y co-ordinate start for drawing cards
	//assiging each card it's position and it's option
	for (var i = 0;i < 13;i++)
	{				
		pair = [player.cardStartPosX + i * (cardWidth / 2),
					player.cardStartPosY + i * 0];
		player.cardPos.push(pair);		
		player.cardHover.push(0);
		player.cardInHand.push(1);
		player.cardView.push(0);
	}		
	//end player 0 options

	//player 1 options
	player = players[1];	
	player.cardStartPosX = - 0.5 * 0.6 * canvasH;
	player.cardStartPosY = - 0.5 * canvasW;
	for (var i = 0;i < 13;i++)
	{	
		var pair = [player.cardStartPosX + i * (cardWidth / 2),
					player.cardStartPosY + i * 0];			 
		player.cardPos.push(pair);
		player.cardHover.push(0);
		player.cardInHand.push(1);
		player.cardView.push(1);
		player.cards.push([-1,-1]);
	}
	//end player 1 options

	//player 2 options
	player = players[2];	
	player.cardStartPosX = 0.3 * canvasW;
	player.cardStartPosY = 0;
	for (var i = 0;i < 13;i++)
	{		
		pair = [player.cardStartPosX + i * (cardWidth / 2),
					player.cardStartPosY + i * 0];
		player.cardPos.push(pair);
		player.cardHover.push(0);
		player.cardInHand.push(1);
		player.cardView.push(1);
		player.cards.push([-1,-1]);
	}	
	//end player 2 options

	//player 3 options
	player = players[3];	
	player.cardStartPosX = -0.5 * 0.6  * canvasH;
	player.cardStartPosY =  0.5 * 0.76 * canvasW;
	for (var i = 0;i < 13;i++)
	{	
		var pair = [player.cardStartPosX + i * (cardWidth / 2),
					player.cardStartPosY + i * 0];			 
		player.cardPos.push(pair);
		player.cardHover.push(0);
		player.cardInHand.push(1);
		player.cardView.push(1);
		player.cards.push([-1,-1]);
	}
	//end player 3 options	
	
	/*
		note: a bad practice here that i used 13 as the loop boundary however this is valid since
		this is the initial step that happens before even distributing cards.

	*/

	/*
		note: to get a certain card from the cards.gif image you take the cardPicRank multiply it by cardHeight to get
		the card picture rank then take the cardNumInPic multiply it by card width to get the number of the card.
	*/

}



/*               =======gameplay funs=======               */


/*
	function that gets the mouse position of the client inside the canvas
*/
function getMousePos(evt) 
{
	var rect = canv.getBoundingClientRect();
	return {
	  x: evt.clientX - rect.left,
	  y: evt.clientY - rect.top
	};
}

/*
	function that gets the card number pressed by player
*/
function getCardNumberP0(mousePos)
{
    //reference to start from.
    var refX = players[0].cardStartPosX, refY = players[0].cardStartPosY;
    
    //margin of cards
    var cardMarginX = 7 * cardWidth, cardMarginY = cardHight;

    /*
    	7 * cardWidth = 12 * (cardWidth) + cardWidth = 14 * (cardWidth / 2) 
    	last card takes 2 cardWidth / 2  (over lapping cards)

    	this gets the space on which the 13 cards of the player are drawn.
    */
    
    if((mousePos.x >= refX && mousePos.x < refX + cardMarginX) && 
       (mousePos.y >= refY && mousePos.y < refY + cardMarginY))
    {
        var cardNo = (mousePos.x - refX) / (cardWidth / 2); 
        cardNo = Math.ceil(cardNo);
        return (cardNo - 1);
    }
    else
    	return -1;
	
}

/*
	update canvas
*/
function drawCanvas()
{	
	
	//first clear the canvas
	ctx.fillStyle="#008000";
	ctx.fillRect(0, 0, canvasW, canvasH);
	ctx.stroke();
		
	//redraw players cards	
	for (var i = 0 ;i < 4;i++)
	{
		players[i].drawPlayerCards();
	}

	//update game log for player.
	gameLog.writeText();
		
}




/*
	function that if given player number moves the card to its position to be played and adds it to played cards.
*/

//	init canvas : add listeners (only work for player zero)
//  listener that moves card on hover.
//  listener that moves card if played.
function initCanvas()
{		

	/* adding listenrs */

	//detecting mouse poistion for hovers (only for player 0)
	canv.addEventListener('mousemove', function(evt) {

        var mousePos = getMousePos(evt);

    	var cardNo = getCardNumberP0(mousePos);    	
    	var player = players[0];
    	if (cardNo != -1 && player.cardInHand[cardNo] == 1)
    	{    		
    		player.cardHover[cardNo] = 1;
    		player.cardPos[cardNo][1] = player.cardStartPosY - (cardWidth / 4) ;    		  	    		

    	}

    	//return hovered cards to their places.
    	for (var i = 0;i < 13;i++)
    	{
    		if(i != cardNo && player.cardInHand[i] == 1)
    		{
    			players[0].cardHover[i] = 0;
    			
    			//pair(x,y) of start position of cards
    			pair = [players[0].cardStartPosX + i * (cardWidth / 2),
					players[0].cardStartPosY + i * 0];
				players[0].cardPos[i] = pair;				
    		}
    	}

    	//update game view.
    	drawCanvas();
    });

    //detecting mouse clicks for playing a card
    canv.addEventListener('mousedown', function(evt) {
    	var mousePos = getMousePos(evt);

		//if the game did not start
      	if (game.gameState != 3)
      	{
      		return;
       	}       	
       	console.log("trying to play, turn ",game.playerNoTurn)
       	//left click was indicated. if valid card, add this card as the next move for player 0
       	if(evt.button == 0 && autobot == 0 && myTurn == 1)
       	{	
   	        var cardNo = getCardNumberP0(mousePos);        
   	        var player;
   	        if (cardNo != -1)
   	        {	
   	        	console.log("captured card : ", cardNo);
   	        	game.lastCardIdxPlayed = cardNo;
   	        	socket.emit('playedCard', cardNo);
   	        }
       	}

       	//in this case we have bots playing  for testing purpose.
       	if(evt.button == 0 && autobot == 1 && playerTurn == 0)
       	{
       		var cardNo = getCardNumberP0(mousePos);        
   	        var player;
   	        if (cardNo != -1)
   	        {	
   	        	console.log("captured card : ", cardNo);
   	        	game.lastCardIdxPlayed = cardNo;
   	        	if (validateMove(0, cardNo) == 1)
   	        	{
	   	        	playerPlaysCard(0, cardNo);
	   	        	playerTurn++;
	   	        	playerTurn %= 4;
	   	        	evaluate();
	   	        }
   	        }	
       	}
    });
    drawCanvas();
}

//=============================== testing suite =========================//
/*
	test the game as if there is a server and the game is going.
*/
//main function to test game.
function testGame()
{
	autobot = 1;
	preparePlayers();
	initCanvas();
	drawCanvas();
	randomPlay();
}

//this function should loop on players turns
function randomPlay()
{

	//TODO : Place a random call here for a random player
	console.log("placing random call");
	
	//picking up a random call from 7 to 13.
	game.lastCall = Math.floor(Math.random() * (13 - 7) + 7);
	
	//picking up a random player.
	game.lastPlayerCall = Math.floor(Math.random() * 3);

	console.log("call is placed");
	console.log("player who called: ", game.lastPlayerCall);
	console.log("player call : ", game.lastCall);

	//TODO : init and shuffle cards
	initGameCards();
	shuffle();

	//TODO : give cards to players
	drawCards();
}


function initGameCards()
{
	game.cards = [];
	for (var i = 0;i < 4;i++)
	{
		for (var j = 0;j < 13;j++)
		{
			game.cards.push([i,j]);
		}
	}

}

function shuffle()
{
	var j, x, i, a = game.cards;
    for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function drawCards()
{
	for (var i = 0; i < 4;i++)
	{
		for (var j = 0;j < 13;j++)
		{
			var tmp = game.cards.shift();			
			players[i].cards.push(tmp);
		}
	}

	//should sort players cards here (should be implemented wisely)
	for (var i = 0; i < 4;i++)
	{
		players[i].cards.sort();
	}
}

function playerPlaysCard(playerNo, cardNo)
{
	for (var count = 0;count < 1500;count++){}
	var player = players[playerNo];
	player.cardPos[cardNo] = midTable[playerNo];
	player.cardView[cardNo] = 0;
	player.cardInHand[cardNo] = 0;
	game.currRoundCards.push(player.cards[cardNo]);
	player.cards.splice(cardNo,1);
	drawCanvas();
}

function validateMove(playerNo, cardNo)
{
	//if no card has been played yet then this is a valid card
	if(game.currRoundCards.length == 0)
		return 1;

	player = players[playerNo];
	playerCards = player.cards;
	
	//if this card is not in my hand then this is invalid card
	if(player.cardInHand[cardNo] == 0) 
		return -1;
	
	gameCards = game.currRoundCards;
	card = playerCards 									//possible bug here should have been card = playerCards[cardNo]

	//if the card has the same color as the base card(first card) then this is a valid card.
	if(gameCards[0][0] == card[0])
		return 1;

	//check if the player has another card that has the same base
	for (var i = 0;i < playerCards.length;i++)
	{
		//if player has another card that has the same base as first card
		if (playerCards[i][0] == gameCards[0][0] && playerCards[i] != card)
			return -1;
	}

	//if card passed all these checks then it is valid.
	return 1;
}

//should evaluate the winner and decide who starts.
function evaluate()
{
	if(game.currRoundCards.length < 4)
		return;
	//getting first player who played. (the player at which the turn is currently at after 4 cards)
	var firstPlayer = game.playerNoTurn
	var maxCard = [-1,-1], maxCardIdx = -1,
	    gameCards = game.currRoundCards, baseColor = game.currRoundCards[0][0], normalRound = 0;
	//loop for all cards. if this card has the same color as base check if it is higher than the current higher card
	//if yes .. set it as the max card. otherwise check if it has the same color as the cut color and check if it is higher than the current card    
	for (var i = 0;i < gameCards.length;i++)
	{
		if(gameCards[i][0] == baseColor) 
		{
			//i relized here that the ACE is card 0 :D which makes the checks with bigger than and smaller than false :D 
			//as a result should implement a compare function.
			if(gameCards[i][1] > maxCard[1] || gameCards[i][1] == '0')
			{	
				maxCard = gameCards[i][1];
				maxCardIdx = i;
			}
		}
		else if(baseColor == game.cutColor)
		{
			if(maxCard[0] == game.cutColor && maxCard[1] < gameCards[i][1])
			{
				maxCard = gameCards[i];
				maxCardIdx = i;
			}
			else
			{
				maxCard = gameCards[i];
				maxCardIdx = i;
			}
		}
	}

	//the player who won the round.
	var playerWon = (firstPlayer + i) % 4;

	players[playerWon].capturesCount++;
	game.currRoundCards = [];
	game.playerNoTurn = playerWon;
	console.log("player ",playerWon," won the round.");
}

//decide which card the bot should play. checks implemented within.
function decideCardToPlay(playerNo)
{
	player = players[playerNo];
	console.log("deciding for ",playerNo);
	console.log("currRoundCards = ",game.currRoundCards.length);
	
	//if it is my turn to play first.	
	console.log("cond : ",game.currRoundCards.length == 0 || game.currRoundCards == null);
	if(game.currRoundCards.length == 0 || game.currRoundCards == null)
	{
		//pick up my highest card of any rank.
		for (var i = 12; i >= 0;i--)
		{
			for (var j = 0;j < player.cards.length; j++)
			{
				if(player.cards[j] != null && player.cards[j][1] == i)
					return j;
			}

		}
	}
	
	var playerCards = player.cards;

	var baseCard = game.currRoundCards[0];
	var baseColor = baseCard[0];
	var cutColor = game.cutColor;
	var cutCard  = [0,0];

	var maxCard = [0,0];
	var minCard = [0,14];
	//calculate my highest card and my lowest card.
	for(var i = 0;i < playerCards.length;i++)
	{
		var card = playerCards[i];
		//if this card has the same color as the base
		if(card[0] == baseColor)
		{
			if(card[1] > maxCard[1])
				maxCard = card;
			if(card[1] < minCard[1])
				minCard = card;
		}
		//if this card is cut
		if(card[0] == cutColor)
		{	
			//pick up the highest cut card.
			if(cutCard[1] < card[1])
				cutCard = card;
		}
	}

	//if i have no card as the base pick a cut if no cut pick any
	if(maxCard == [0,0] || minCard == [0,0])
	{
		//i have no cut card
		if(cutCard == [0,0])
		{
			//pick up the lowest card (possible bug: Ace = 1 and it is not the lowest card)
			for (var i = 0;i < 13;i++)
			{
				if(playerCards[i][1] == (i+1))
					return i;
			}
		}
		//else return cut card
		for (var i = 0;i < playerCards.length;i++)
		{
			if (playerCards[i] == cutCard)
				return i;
		}
	}

	//i have a card. lets see whether to return highest or lowest
	var gameCards = game.currRoundCards;
	var cardToPlay = [0,0];
	for(var i = 0;i < gameCards.length;i++)
	{
		if(gameCards[i][1] > maxCard[1])
		{
			cardToPlay = minCard;
			break;
		}
		else 
		{
			cardToPlay = maxCard;
		}		
	}

	//get the index of card and return it.
	for (var i = 0;i < player.cards.length;i++)
	{
		if(player.cards[i] == cardToPlay)
			return i;
	}
}	



setInterval(function(){
	if (autobot == 0)
		return;

	if(game.gameState == 1 && playerTurn != 0)
	{
		decideCardToPlay(playerTurn);
		playerTurn++;
		playerTurn %= 4;
	}

},500)


//========================================================================//
testGame();



