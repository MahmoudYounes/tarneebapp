/**
 * js file that contains info for game tarneeb on server.
 *
 */

var events = require('events');

//game data (should be found on server and read from there)
function game(p1, p2, p3, p4)
{	
		this.gameID			= -1;				// game identified by this gameID.
		this.roundID 		= [];				// set of rounds currently in a game
		this.teamsScore		= [0, 0];			// holds the score for the players
		this.players		= [p1, p2, p3, p4];	// holds players objs.
		this.gameState		= 0;				// 0 => not Started, 1 => currently playing (decide cuts), 2 => players take turns to play, 3 => done decide who wins.
		this.cutColor 		= 4;				// 0 => clubs, 1 => diamond, 2 => heart, 3 => spade, 4 => none.
		this.roundNo		= 0;				// preserves current round no. (0->12: 13 rounds)
		this.playerNoTurn 	= 0;				// preserves the current player turn. (gets current player number)
		this.lastPlayerCall	= 0;				// preserves the last player who called.
		this.passes			= 0;
		this.playerToCall	= 0;
		this.playerLastCapture = 0;
		
		this.lastCall 		= 0;				// preserves the maximum count of calls
		//this.callRounds		= 0;				// preservers the calling rounds to make.
		this.cards 			= [];				// store the cards at the begining
		this.currRoundCards = [];				// store cards in current round
		
		this.gameEvents 	= new events.EventEmitter();
		this.updateGame();

		//important to access this object from callback scope.
		var self = this;

		//initializing players the naive way. (due to facing errors when defining with for loops)
		//player 0
		//capture player calls
		self.players[0].client.on('calling', function(data){
	        //update the game.
	        self.updateGame();

	        if(data[0] == 1 && data[1] != null)
	        {
	        	console.log("setting data.");
	        	if(data[1] > self.lastCall)
	        	{
	        		console.log("player ", 0, " calling: ", data);
	        		self.lastCall = Number(data[1]);
	        		self.lastPlayerCall = 0;
	        		self.passes = 0;
	        	}
	        	else
	        	{
	        		self.gameEvents.emit('move');
	        		return;
	        	}
	        }
	        else
	        {
	        	console.log("player passed");
	        	self.passes++;
	        }

	        //next Player to call.
	        self.playerToCall = 1;


	        if(self.lastCall != 0 && self.passes == 3)
	        {
	        	self.gameState = 2;
	        }
	        else if(self.passes >= 4)
	        {
	        	self.gameState = 0;
	        }
        	
        	
        	self.gameEvents.emit('move');
    	});

		//captures player cut color
		self.players[0].client.on('playerCutColor',function(data){
			//security check that this is the player's call.
			if(self.lastPlayerCall != 0)
			{
				self.gameEvents.emit('move');
				return;
			}

			self.cutColor = data;
			self.playerNoTurn = 0;
			self.gameState = 3;

			console.log("starting game with the following: ");
			console.log("calls         : ", self.lastCall);
			console.log("player Called : ", self.lastPlayerCall);
			console.log("playerNoTurn  : ", self.playerNoTurn);
			console.log("cutColor 	   : ", self.cutColor);

			self.updateGame();
			self.gameEvents.emit('move');
		});

		//captures player trying to play a card.
		self.players[0].client.on('playedCard', function(obj){
			var validity = self.validateMove(0, obj);
			console.log("player 0 tried to play card with validity: ", validity);

			if(validity == 1)
			{
				//TODO: add card to game cards.
				self.currRoundCards.push([self.players[0].cards[obj][0], self.players[0].cards[obj][1], 0]);
				
				//TODO: remove card from player's hand
				self.players[0].cardsInHand[obj] = 0;
					
				//TODO: report to Client that it is a valid move.
				self.sendAllPlayers('validCard', [0, obj, self.players[0].cards[obj]]);
				
				//TODO: advance player Turn.
				self.playerNoTurn++;
				self.playerNoTurn %= 4;
			}
			
			//TODO: not valid or valid should just go back to move.
			self.gameEvents.emit('move');
			
		});
		//end player 0

		//player 1
		//captures player calls
		this.players[1].client.on('calling', function(data){
	        
	        if(data[0] == 1 && data[1] != null)
	        {
	        	console.log("setting data.");
	        	if(data[1] > self.lastCall)
	        	{
	        		console.log("player ", 1, " calling: ", data);
	        		self.lastCall = Number(data[1]);
	        		self.lastPlayerCall = 1;
	        		self.passes = 0;
	        	}
	        	else
	        	{
	        		self.gameEvents.emit('move');
	        		return;
	        	}
	        }
	        else
	        {
	        	console.log("player passed");
	        	self.passes++;
	        }

	        //next Player to call.
	        self.playerToCall = 2;
	        console.log("next player to call ", self.playerToCall);

	       
	        
	        if(self.lastCall != 0 && self.passes == 3)
	        {
	        	self.gameState = 2;
	        }
	        else if(self.passes >= 4)
	        {
	        	self.gameState = 0;
	        }
        	
        	self.gameEvents.emit('move');
    	});

		//captures player cut color
    	self.players[1].client.on('playerCutColor',function(data){
			//security check that this is the player's call.
			if(self.lastPlayerCall != 1)
			{
				self.gameEvents.emit('move');
				return;
			}
			
			self.cutColor = data;
			self.playerNoTurn = 1;
			self.gameState = 3;

			console.log("starting game with the following: ");
			console.log("calls         : ", self.lastCall);
			console.log("player Called : ", self.lastPlayerCall);
			console.log("playerNoTurn  : ", self.playerNoTurn);
			console.log("cutColor 	   : ", self.cutColor);

			self.updateGame();
			self.gameEvents.emit('move');
		});

		//captures player trying to play a card.
		self.players[1].client.on('playedCard', function(obj){
			var validity = self.validateMove(1, obj);
			console.log("player 1 tried to play card with validity: ", validity);
			if(validity == 1)
			{
				//TODO: add card to game cards.
				self.currRoundCards.push([self.players[1].cards[obj][0], self.players[1].cards[obj][1], 1]);
				
				//TODO: remove card from player's hand
				self.players[1].cardsInHand[obj] = 0;
				
				//TODO: report to Client that it is a valid move.
				self.sendAllPlayers('validCard', [1, obj, self.players[1].cards[obj]]);
				
				//TODO: advance player Turn.
				self.playerNoTurn++;
				self.playerNoTurn %= 4;
			}
			
			//TODO: not valid or valid should just go back to move.
			self.gameEvents.emit('move');
		});
		//end player 1

		//player 2
		//captures player calling.
    	this.players[2].client.on('calling', function(data){

	        if(data[0] == 1 && data[1] != null)
	        {
	        	console.log("setting data.");
	        	if(data[1] > self.lastCall)
	        	{
	        		console.log("player ", 2, " calling: ", data);
	        		self.lastCall = Number(data[1]);
	        		self.lastPlayerCall = 2;
	        		self.passes = 0;
	        	}
	        	else
	        	{
	        		self.gameEvents.emit('move');
	        		return;
	        	}
	        }
	        else
	        {
	        	console.log("player passed");
	        	self.passes++;
	        }


	        //next Player to call.
	        self.playerToCall = 3;

	        
	        
	        if(self.lastCall != 0 && self.passes == 3)
	        {

	        	self.gameState = 2;
	        }
	        else if(self.passes >= 4)
	        {
	        	self.gameState = 0;
	        }
        	
        	self.gameEvents.emit('move');
    	});

    	//captures player cut color
    	self.players[2].client.on('playerCutColor',function(data){
			//TODO: implement security check that this is the player's call.
			if(self.lastPlayerCall != 2)
			{
				self.gameEvents.emit('move');
				return;
			}
			
			self.cutColor = data;
			self.gameState = 3;
			self.playerNoTurn = 2;

			console.log("starting game with the following: ");
			console.log("calls         : ", self.lastCall);
			console.log("player Called : ", self.lastPlayerCall);
			console.log("playerNoTurn  : ", self.playerNoTurn);
			console.log("cutColor 	   : ", self.cutColor);

			self.updateGame();
			self.gameEvents.emit('move');
		});

    	//captures player trying to play a card.
		self.players[2].client.on('playedCard', function(obj){
			var validity = self.validateMove(2, obj);
			console.log("player 2 tried to play card with validity: ", validity);
			if(validity == 1)
			{
				//TODO: add card to game cards.
				self.currRoundCards.push([self.players[2].cards[obj][0], self.players[2].cards[obj][1], 2]);
				
				//TODO: remove card from player's hand
				self.players[2].cardsInHand[obj] = 0;
				
				//TODO: report to Client that it is a valid move.
				self.sendAllPlayers('validCard', [2, obj, self.players[2].cards[obj]]);
				
				//TODO: advance player Turn.
				self.playerNoTurn++;
				self.playerNoTurn %= 4;
			}

			//TODO: not valid or valid should just go back to move.
			self.gameEvents.emit('move');
		});
    	//end player 2

    	//player 3
    	//captures player calling.
    	this.players[3].client.on('calling', function(data){
	        if(data[0] == 1 && data[1] != null)
	        {
	        	console.log("setting data.");
	        	if(data[1] > self.lastCall)
	        	{
	        		console.log("player ", 3, " calling: ", data);
	        		self.lastCall = Number(data[1]);
	        		self.lastPlayerCall = 3;
	        		self.passes = 0;
	        	}
	        	else
	        	{
	        		self.gameEvents.emit('move');
	        		return;
	        	}
	        }
	        else
	        {
	        	console.log("player passed");
	        	self.passes++;
	        }


	        //next Player to call.
	        self.playerToCall = 0;

	        
	        
	        if(self.lastCall != 0 && self.passes == 3)
	        {
	        	self.gameState = 2;
	        }
	        else if(self.passes >= 4)
	        {
	        	self.gameState = 0;
	        }
        	
        	self.gameEvents.emit('move');
    	});

    	//captures player cut color.
    	self.players[3].client.on('playerCutColor',function(data){
			//TODO: implement security check that this is the player's call.
			if(self.lastPlayerCall != 3)
			{
				self.gameEvents.emit('move');
				return;
			}
			
			self.cutColor = data;
			self.playerNoTurn = 3;
			self.gameState = 3;

			console.log("starting game with the following: ");
			console.log("calls         : ", self.lastCall);
			console.log("player Called : ", self.lastPlayerCall);
			console.log("playerNoTurn  : ", self.playerNoTurn);
			console.log("cutColor 	   : ", self.cutColor);

			self.updateGame();
			self.gameEvents.emit('move');
		});

		//captures player trying to play a card.
		self.players[3].client.on('playedCard', function(obj){
			var validity = self.validateMove(3, obj);
			console.log("player 3 tried to play card with validity: ", validity);
			if(validity == 1)
			{
				//TODO: add card to game cards.
				self.currRoundCards.push([self.players[3].cards[obj][0], self.players[3].cards[obj][1], 3]);
				
				//TODO: remove card from player's hand
				self.players[3].cardsInHand[obj] = 0;

				//TODO: report to Client that it is a valid move.
				self.sendAllPlayers('validCard', [3, obj, self.players[3].cards[obj]]);
				
				//TODO: advance player Turn.
				self.playerNoTurn++;
				self.playerNoTurn %= 4;
			}
			
			//TODO: not valid or valid should just go back to move.
			self.gameEvents.emit('move');
		});
		//end player 3

		self.gameEvents.on('init', function(){
			self.cards = [];
			self.playerNoTurn 	= 0;				// preserves the current player turn. (gets current player number)
			self.passes			= 0;
			self.playerToCall	= 
			(self.lastPlayerCall + 1) % 4;			// current player to call
			
			self.lastCall 		= 0;				// preserves the maximum count of calls
			//initializing cards
			for (var i = 0;i < 4;i++)
			{
				for (var j = 0;j < 13;j++)
				{
					self.cards.push([i,j]);
				}
			}
		
			//shuffling cards.
			var j, x, i, a = self.cards;
		    for (i = a.length; i; i -= 1) 
		    {
		        j = Math.floor(Math.random() * i);
		        x = a[i - 1];
		        a[i - 1] = a[j];
		        a[j] = x;
		    }

		    //giving cards
		    for (var i = 0; i < 4;i++)
			{	
				self.players[i].cards = [];
				self.players[i].cardsInHand = [];
				self.players.capturesCount = 0;
				for (var j = 0;j < 13;j++)
				{
					var tmp = self.cards.shift();			
					self.players[i].cards.push(tmp);
					self.players[i].cardsInHand.push(1);
				}
			}
			//sorting cards
			//should sort players cards here (should be implemented wisely)
			for (var i = 0; i < 4;i++)
			{
				self.players[i].cards.sort();
			}

			//sending cards to clients.
			for (var i = 0;i < 4;i++)
			{
				self.players[i].send('recieveCards', self.players[i].cards);
			}

			self.gameState = 1;
			self.gameEvents.emit('move');

		});
			
			

		this.gameEvents.on('move', function(){
			console.log("moving");
			console.log(self.gameState);

			//updating the game.
			self.updateGame();

			if(self.gameState == 0)
			{
				self.gameEvents.emit('init');
			}
			//calling state.
			else if(self.gameState == 1)
			{
				self.sendAllPlayers('getCall',[0]);
			}
			//deciding cut color.
			else if(self.gameState == 2)
			{
				self.sendAllPlayers('decideCutColor', self.lastPlayerCall);
			}
			//taking turns to play
			else if(self.gameState == 3)
			{
				//if already 4 cards present on the table evalute this round
				if (self.currRoundCards.length == 4)
				{
					self.evaluate();
					self.currRoundCards = [];
					self.updateGame();
				}

				//if this is the thirteenth round go to next state which is evaluate score.
				if (self.roundNo == 13)
				{
					self.gameState = 4;
					self.updateGame();
					self.gameEvents.emit('move');
					return;
				}

				//ask players to play.
				self.sendAllPlayers('takeTurns', self.playerNoTurn);
			}
			//evaluating round and check if there is another round.
			else if(self.gameState == 4)
			{
				//getting scores for each team.
				var team0 = self.players[0].capturesCount + self.players[2].capturesCount;
				var team1 = self.players[1].capturesCount + self.players[3].capturesCount;

				//player 0 or 2 placed the call.
				if(self.playerToCall == 0 || self.playerToCall == 2)
				{
					if(team0 >= self.lastCall)
					{
						self.teamsScore[0] += team0;
					}
					else
					{
						self.teamsScore[0] -= self.lastCall;
					}
				}
				else
				{
					if(team1 >= self.lastCall)
					{
						self.teamsScore[1] += team1;
					}
					else
					{
						self.teamsScore[1] -= self.lastCall;
					}
				}

				if(self.teamsScore[0] >= 31)
				{
					self.sendAllPlayers('teamWon', [0,2]);
				}
				if(self.teamsScore[1] >= 31)
				{
					self.sendAllPlayers('teamWon', [1,3]);
				}
				console.log("t0: ",self.teamsScore[0]);
				console.log("t1: ",self.teamsScore[1]);
				
				self.gameState = 0;
				self.updateGame();
				self.gameEvents.emit('init');


			}
		});

		this.gameEvents.emit('move');


}

game.prototype.updateGame = function()
{
	var obj = {
		gameState 		: this.gameState,
		cutColor  		: this.cutColor, 		
		roundNo   		: this.roundNo,
		lastPlayerCall 	: this.lastPlayerCall,
		lastCall 		: this.lastCall,
		playerToCall    : this.playerToCall,
		playerNoTurn    : this.playerNoTurn,
		currRoundCards  : this.currRoundCards,
		playerLastCapture : this.playerLastCapture,
		teamsScore 		: this.teamsScore,
		pCap0 			: this.players[0].capturesCount,
		pCap1 			: this.players[1].capturesCount,
		pCap2 			: this.players[2].capturesCount,
		pCap3 			: this.players[3].capturesCount,
	}
	this.sendAllPlayers('updateGame' , obj);
}

game.prototype.sendAllPlayers = function(evtName, obj)
{
	this.players.forEach(function(player){
		player.send(evtName, obj);
	});
}

game.prototype.validateMove = function (playerNo, cardNo)
{
	player = this.players[playerNo];
	playerCards = player.cards;

	//this condition due to UI problem. (captures card 13 which is undefined).
	if(cardNo > 12) return -1;
	
	//if this card is not in my hand then this is invalid card
	if(player.cardsInHand[cardNo] == 0) 
		return -1;

	//if no card has been played yet then this is a valid card
	if(this.currRoundCards.length == 0)
		return 1;
	
	gameCards = this.currRoundCards;
	card = playerCards[cardNo];

	console.log("gc: ", gameCards[0]);
	console.log("c: ",  card);

	//if the card has the same color as the base card(first card) then this is a valid card.
	if(gameCards[0][0] == card[0])
		return 1;

	//check if the player has another card that has the same base
	for (var i = 0;i < playerCards.length;i++)
	{
		//if player has another card that has the same base as first card
		if (player.cardsInHand[i] == 1 && playerCards[i][0] == gameCards[0][0] && playerCards[i] != card)
			return -1;
	}

	//if card passed all these checks then it is valid.
	return 1;
}

//should evaluate the winner and decide who starts.
game.prototype.evaluate = function()
{
	//getting first player who played. (the player at which the turn is currently at after 4 cards)
	var maxCard = this.currRoundCards[0], maxCardIdx = -1,
	    gameCards = this.currRoundCards, baseColor = this.currRoundCards[0][0], normalRound = 0;
	
	console.log(gameCards);

	//loop for all cards. if this card has the same color as base check if it is higher than the current higher card
	//if yes .. set it as the max card. otherwise check if it has the same color as the cut color and check if it is higher than the current card    
	for (var i = 1;i < gameCards.length;i++)
	{
		if(gameCards[i][0] == baseColor) 
		{
			//i relized here that the ACE is card 0 :D which makes the checks with bigger than and smaller than false :D 
			//as a result should implement a compare function.
			if(this.compareCards(gameCards[i], maxCard) == 0)
			{	
				maxCard = gameCards[i];
				maxCardIdx = i;
				console.log(maxCard);
			}
		}
		else if(gameCards[i][0] == this.cutColor)
		{
			if(maxCard[0] == this.cutColor && (maxCard[1] != 0 && maxCard[1] < gameCards[i][1]))
			{
				maxCard = gameCards[i];
				maxCardIdx = i;
				console.log(maxCard);
			}
			else if(maxCard[0] != this.cutColor)
			{
				maxCard = gameCards[i];
				maxCardIdx = i;
				console.log(maxCard);
			}
		}
	}

	console.log("2 ",maxCard);
	//the player who won the round.
	var playerWon = maxCard[2];

	this.players[playerWon].capturesCount++;
	this.playerNoTurn = playerWon;
	this.playerLastCapture = playerWon;
	this.roundNo++;
	this.sendAllPlayers('newRound', [0]);
}

game.prototype.compareCards = function(c1, c2){
	if(c1[0] != c2[0]) return -1; // can't compare that.
	
	if(c1[1] == 0 ) return 0; //c1 is greater.
	
	if(c2[1] == 0) return 1;  //c2 is greater
	
	if(c1[1] > c2[1]) return 0;

	if(c2[1] > c1[1]) return 1;
}

module.exports = game;