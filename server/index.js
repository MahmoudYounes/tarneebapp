/**
*	//simple app the prints hello world.
*	var app = require('express')();
*	var http = require('http').Server(app);
*	
*	//whenever a get request is detected for the home directory.
*	app.get('/', function(req, res){
*	  res.send('<h1>Hello world</h1>');
*	});
*	
*	http.listen(3000, function(){
*	  console.log('listening on *:3000');
*	});
****************************************************************************************
//should be used to send the script files to the client connecting to the url.
*app.get('/', function(req, res){
*  res.sendFile(__dirname + '/index.html');
*});
****************************************************************************************
//should be used to detect if a client is connected.
*io.on('connection', function(socket){
*  console.log('a user connected');
*});
****************************************************************************************
//makes the server listen on a certain port.
http.listen(3000, function(){
  console.log('listening on *:3000');
});
**/






//initial server modules
var tarneebServer = require('express')();
var http = require('http').Server(tarneebServer);
var io = require('socket.io')(http);
var game = require('./game.js');
var player = require('./player.js');

//in game vars
var playersID = 0;				//used to keep track of players ID.
var lobbyArray = [];			//used to preserve connected players till 4 are present then start a new game.

var games = [];
//logs user connection.
io.on('connection', function(client){


	//handling disconnections
	client.on("disconnect", function(){
		//logging action
		console.log(connectedPlayer.id, " is disconnected.");

		//removing client from lobby array.
		for(var i = 0;i < lobbyArray.length;i++)
		{
			if(lobbyArray[i].id == connectedPlayer.id)
			{
				lobbyArray.splice(i,1);
				break;
			}
		}
	});
	
	//creating player for the connected client.
	var connectedPlayer = new player(lobbyArray.length, client);

	//pushing the player in the lobby.
	lobbyArray.push(connectedPlayer);
	
	//logging the action.
	console.log("a user connected with id : ", client.id);
	
	//sending first message which conatains pActualNumber.
	client.emit('firstConnection', lobbyArray.length - 1);

	//checking if four players exist in the lobby to start the game.
	if(lobbyArray.length == 4)
	{		
		//TODO: start a new game for the new four players.
		var newGame = new game(lobbyArray[0], lobbyArray[1], lobbyArray[2], lobbyArray[3]);
		games.push(newGame);

		lobbyArray = [];
		
	}

});

//server listens on a certain port
http.listen(80, function(){
  console.log('listening on *:80');
});


games.forEach(function(gdata){
	console.log(gdata);
});

/*setInterval(function(){
	for(var i = 0;i < games.length;i++)
	{
		games[i].gameEvents.emit('move');	
	}
},1000);*/

