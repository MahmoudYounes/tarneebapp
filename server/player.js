module.exports = Player;
function Player (plNum, client) 
{
    this.client = client;
    this.id = plNum;

    this.cards = [];
    this.cardsInHand = [];
    this.gameID = -1;
    this.lastCall = [];
    this.capturesCount = 0;
}

//returns playerID
Player.prototype.getID = function()
{
    return this.id;
};

//returns polaID
Player.prototype.getPolaID = function()
{
    return this.polaID;
};

//cards? or card :D ? akeed card :D 
Player.prototype.addCard = function (card) 
{
    this.cards.push(card);
};

//removing a certain card :D.
Player.prototype.rmvCard= function(card,index)
{
    //var findex = 0;
    for(var i = 0;i < this.cards.length;i++)
    {
        if(this.cards[i] == card)
        {
            this.cards.splice(i,1);
        }
    }
};

Player.prototype.cardIsFound = function(card)
{
    for (var i = 0; i < this.cards.length; i++) 
    {

        if (this.cards[i][0] == card[0] && this.cards[i][1] == card[1]) 
        {
            return i;   // Found it
        }
    }
    return -1;   // Not found
};

Player.prototype.colorIsFound = function(color) 
{
	// body...
	for (var i = 0; i < this.cards.length; i++) 
    {
    	if (this.cards[i][0] == color ) 
        {
        	return true;   // Found it
    	}
	}
	return false;   // Not found	
};


//functions that sends object to this client under certain event.
Player.prototype.send = function(evtName, obj)
{
    this.client.emit(evtName, obj);
}

